const express = require("express");

// Here we use : .put .get .post
// app.get()
// request.method == "GET"

// express.Router() method allows access to HTTP Method
const router = express.Router();

const taskControllers = require("../controllers/taskControllers"); // pwede lg wala JS

/*
// Concept is same like This property:

taskControllers = {
    createTaskControllers: function(){..}
    anotherController: function(){..}
}

*/

// Create - task routes

router.post("/addTask", taskControllers.createTaskController);

// Get all task
router.get("/allTasks", taskControllers.getAllTasksController);

// Delete specific task
router.delete("/deleteTask/:taskId", taskControllers.deleteTaskController);

// For Activity
router.get("/:taskId", taskControllers.getTaskController);

router.put("/:taskId/complete", taskControllers.updateTaskController);

// Change the status of a task to "complete"
// This route expects to receive a put request at the URL "/tasks/:id/complete"
// The whole URL is at "http://localhost:3001/tasks/:id/complete"
// We cannot use put("/tasks/:id") again because it has already been used in our route to update a task
router.put("/:id/archive", (req, res) => {
	taskControllers.changeNameToArchive(req.params.id).then(resultFromController => res.send(resultFromController));
}) 

router.patch("/updateTask/:taskId", taskControllers.updateTaskNameController);

module.exports = router;


